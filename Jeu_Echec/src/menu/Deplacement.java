package menu;

//repr�senter un d�placement sur l'�chiquier
public class Deplacement {
	
	private double depX;
	
	private double depY;
	
	private Position arrivee;
	
	private Position depart;

	public Deplacement(Position depart , Position arrivee) {

		this.arrivee = arrivee;
		this.depart = depart;
		this.depX = arrivee.getColonne() - depart.getColonne();
		this.depY = arrivee.getLigne() - depart.getLigne();		
	}

	public double getDepX() {
		return depX;
	}

	public double getDepY() {
		return depY;
	}

	public Position getArrivee() {
		return arrivee;
	}

	public Position getDepart() {
		return depart;
	}
	
	public boolean Pasdep() {
		return depX == 0 && depY == 0;
	}


}
