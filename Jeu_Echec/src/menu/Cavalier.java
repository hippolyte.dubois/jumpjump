package menu;

public class Cavalier extends Piece {	
	
	public Cavalier(String color) {
		super("Cavalier", color);
	}
	
	public boolean estValide(Deplacement deplacement) {
		return ((deplacement.getDepX() == -1 || deplacement.getDepX() == 1) && (deplacement.getDepY() == 2 || deplacement.getDepY() == -2)) || 
				((deplacement.getDepX() == -2 || deplacement.getDepX() == 2) && (deplacement.getDepY() == 1 || deplacement.getDepY() == -1));
	}

}
