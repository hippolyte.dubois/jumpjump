package menu;

import javax.swing.*;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

// Menu si on veut jouer contre une IA
public class MenuIA extends JFrame {
	
	private JPanel pand;
	private JPanel pang;
	private JButton btnRetour;
	private JButton btnSituation;
	private JButton btnPartiecontreIA;
	private String langue;
	
	public MenuIA(String langue) {
		this.langue=langue;
		setTitle("Chess Game");//donne un titre
		setSize(400, 350);//400px/350px
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		pand = new JPanel();
		pand.setLayout(new GridLayout(2,0));
		pand.add(new JLabel("resolver des situations pour avancer"));
		pand.add(btnSituation = new JButton("Situation"));
		
		pang = new JPanel();
		pang.setLayout(new GridLayout(2,0));
		pang.add(new JLabel("Mesurez vous contre une IA "));
		pang.add(btnPartiecontreIA = new JButton("Jouer"));
		
		setLayout(new GridLayout(3,0));
		add(pand);
		add(pang);
		add(btnRetour = new JButton("Retour"));
		
		this.setVisible(true);
		
		GestionnaireEvent gest = new GestionnaireEvent();	
		btnRetour.addMouseListener(gest);
		btnSituation.addMouseListener(gest);
		btnPartiecontreIA.addMouseListener(gest);
	}

	private void jouer() {
		if(JOptionPane.showConfirmDialog(null, " Cette option n'est pas encore disponible \n", "En cours de d�veloppement !", JOptionPane.DEFAULT_OPTION) == 0) {
			
		}
	}

	private void retour() {
		this.dispose();
		new Menu(langue);
	}
	
	
	public class GestionnaireEvent extends MouseAdapter{
		

		public void mouseClicked(MouseEvent eve) {
			
			if (eve.getSource() == btnRetour){
				retour();	
			}
			else if (eve.getSource()== btnPartiecontreIA) {
				jouer();
			}
			else if (eve.getSource()== btnSituation) {
				jouer();
			}
			
		}
	}
}


