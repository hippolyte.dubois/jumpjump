package menu;


public class Pion extends Piece {

	
	public Pion(String color) {
		super("Pion", color);
	}
	
	public boolean estValide(Deplacement deplacement) {
		
		if (deplacement.getDepX() == 0) {	
			if (this.getColor().equals("noir")){
				  
				return deplacement.getDepY() <= (deplacement.getDepart().getLigne() == 1 ? 2 : 1) && deplacement.getDepY() > 0;
			}
			else 
				return deplacement.getDepY() >= (deplacement.getDepart().getLigne() == 6 ? -2 : -1) && deplacement.getDepY() < 0;		
			
		}

		return false;				
	}
}