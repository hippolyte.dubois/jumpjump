package menu;

public class Dame extends Piece {	
	public Dame(String color) {
		super("Dame", color);
	}
	public boolean estValide(Deplacement deplacement) {
			return (Math.abs(deplacement.getDepX()) == Math.abs(deplacement.getDepY()) ||
					deplacement.getDepX() == 0 || deplacement.getDepY() == 0) && !deplacement.Pasdep();
	}
}
