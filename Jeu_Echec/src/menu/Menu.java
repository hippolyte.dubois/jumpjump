package menu;
import javax.swing.*;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

// Menu principale du jeu
public class Menu extends JFrame{
	
	//les variables utiles
	private JPanel pan;
	private JButton btnJvJ;
	private JButton btnJvIA;
	private JButton btnRegle;
	private JButton btnLangue;
	private JButton btnQuitter;
	
	

	public String langue;
	
	public Menu (String langue)
	{
		this.langue=langue;
		setTitle("Chess Game");//donne un titre
		setSize(400, 350);//400px/350px
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		pan = new JPanel();
		pan.setLayout(new GridLayout(5,0));
		pan.add(btnJvJ = new JButton("Joueur vs Joueur"));
		pan.add(btnJvIA = new JButton("Joueur vs IA"));
		pan.add(btnRegle = new JButton("R�gle"));
		pan.add(btnLangue = new JButton("Langue"));
		pan.add(btnQuitter = new JButton("Quitter"));
		
		add(pan);
		
		//on ajoute les actions li� aux boutons
		GestionnaireEvent gest = new GestionnaireEvent();		
		btnJvJ.addMouseListener(gest);
		btnJvIA.addMouseListener(gest);
		btnRegle.addMouseListener(gest);
		btnLangue.addMouseListener(gest);
		btnQuitter.addMouseListener(gest);

		setVisible(true);
		
	}
	
	
	public void accesRegle() throws IOException{
		this.dispose();
		new MenuRegle(langue);

	}
	public void accesLangue() {
		this.dispose();
		new MenuLangue(langue);
	}
	public void accesJvJ() {
		this.dispose();
		new AfficherEchiquier(langue);
	}
	
	public void accesJvIA() {
		this.dispose();
		new MenuIA(langue);
	}
	
	public void quitter() {
		this.dispose();
	}
		
	
	//class qui s'occupe de g�rer les actions des boutons
	public class GestionnaireEvent extends MouseAdapter{
		
		public void mouseClicked(MouseEvent eve) {
			
			if (eve.getSource() == btnQuitter){
				quitter();
			}
			else if(eve.getSource() == btnJvJ) {
				accesJvJ();
			}
			else if(eve.getSource() == btnJvIA) {
				accesJvIA();
			}
			else if (eve.getSource() == btnLangue) {
				accesLangue();
			}
			else if (eve.getSource() == btnRegle) {
				try {
					accesRegle();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
