package menu;

import javax.swing.border.EtchedBorder;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;



// afficher un �chiquier
public class AfficherEchiquier extends JFrame {


	private Echiquier e; 
	private JLabel[][] tab;
	private JButton btnRetour = new JButton("Quitter la partie");
	private JPanel panelGrille = new JPanel(); 
	GridLayout gridLayout1 = new GridLayout();
	private String dossierIcone = "src/pi�ce/";
	private String langue;
	
	public AfficherEchiquier(String l) {
		langue = l;
		e = new Echiquier();
		tab = new JLabel[8][8];
		GestionnaireEvent gest = new GestionnaireEvent();
		this.setTitle("Chess Game");//donne un titre
		this.setSize(600, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.getContentPane().add(panelGrille, null);
		
		panelGrille.setBounds(new Rectangle(5, 65, 550, 465));
		panelGrille.setBorder(BorderFactory
				.createEtchedBorder(EtchedBorder.RAISED));
		panelGrille.setLayout(gridLayout1);
		gridLayout1.setColumns(8);
		gridLayout1.setRows(8);
		
		int a = 1;
		for (int ligne = 0; ligne < 8; ligne++) {
			a = a == 1 ? 0 : 1;
			for (int colonne = 0; colonne < 8; colonne++) {
				tab[colonne][ligne] = new JLabel(); 
				tab[colonne][ligne].setOpaque(true);
				panelGrille.add(tab[colonne][ligne]); 
				tab[colonne][ligne].setOpaque(true);
				tab[colonne][ligne].setHorizontalAlignment(SwingConstants.CENTER); 
				tab[colonne][ligne].addMouseListener(gest); 
				if ((colonne + 1) % 2 == a)
					tab[colonne][ligne].setBackground(new Color(255, 255, 255));
				else
					tab[colonne][ligne].setBackground(new Color(100, 100, 100));
				}
		}
		btnRetour.addMouseListener(gest);
		
		this.setLayout(new BorderLayout(2,0));
		this.add(panelGrille, BorderLayout.CENTER);
		this.add(btnRetour,BorderLayout.SOUTH);
		
		disppiont();
		this.setVisible(true);
	}
	

	public void disppiont() {
		
		e.initialiser(); 

		String[] ordrePiece = { "T", "C", "F", "D", "R", "F", "C", "T" };
		int increment = 1;
		int ligne = 0;
		char couleur = 'N';
		Piece tempo = null;
		e.initialiser(); // code


		while (increment >= -1) {
			for (int ctr = 0; ctr <= 7; ctr++) {
				tab[ctr][ligne].setIcon(new ImageIcon(dossierIcone+ordrePiece[ctr]+couleur+".gif"));
				switch(ordrePiece[ctr])
				{
				case "T":
					tempo = new Tour(ligne < 5 ? "noir" : "blanc");
				break;
				
				case "C":
					tempo = new Cavalier(ligne < 5 ? "noir" : "blanc");
				break;
				
				case "F":
					tempo = new Fou(ligne < 5 ? "noir" : "blanc");
				break;
				
				case "D":
					tempo = new Dame(ligne < 5 ? "noir" : "blanc");
				break;
				
				case "R":
					tempo = new Roi(ligne < 5 ? "noir" : "blanc");
				break;
				}
				e.getCase(ctr, ligne).setPiece(tempo);
				tab[ctr][ligne + increment].setIcon(new ImageIcon(dossierIcone+"P"+couleur+".gif"));
				e.getCase(ctr, ligne + increment).setPiece(new Pion(ligne < 5 ? "noir" : "blanc"));
				
			}
			couleur = 'B';
			increment -= 2;
			ligne = 7;
		}
	}
	
	public void retour() {
		this.dispose();
		new Menu(langue);
	}
	
	public class GestionnaireEvent extends MouseAdapter{
		
		Piece piDoubleClick = null;//piece qui est double cliqu�
		int ligneClic2;//pour double cliquer
		int colonneClic2;//pour double cliquer
		Position poDoubleClick = null;
		Piece pieceTampon = null;
		ImageIcon iconeTampon;
		int ligneClic;
		int colonneClic;
		Position depart, arrivee;
		String couleurControle = "blanc";
		Position temp = null;

		
		public void mouseClicked(MouseEvent eve) { 
			
			if (eve.getSource() == btnRetour) {
				if(JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter? \n", "Chess Game", JOptionPane.YES_NO_OPTION) == 0){
					retour();
				}
					
			}
			
			
			else if (eve.getSource() instanceof JLabel) // donc on a cliqu� sur un Label
			{
				//sur quel label on clique
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						if (eve.getSource() == tab[j][i]) {
							ligneClic = i;
							colonneClic = j;
						}
					}
				}
				
				// clique sur une case vide et tampon null
				if((e.getCase(colonneClic, ligneClic).getPiece() != null | pieceTampon != null) ){

						
					//tampon non null
					if(pieceTampon == null ){
						// si la pi�ce est de la couleur de couleurControle
						if(e.getCase(colonneClic, ligneClic).getPiece().getColor().equals(couleurControle)){
								
								//initialisation du tampon
								pieceTampon = e.getCase(colonneClic, ligneClic).getPiece();
								iconeTampon = (ImageIcon)tab[colonneClic][ligneClic].getIcon();
								temp = new Position(colonneClic,ligneClic);
								tab[colonneClic][ligneClic].setBorder(BorderFactory.createLineBorder(new Color(255, 0, 0),5));
							}
							
						}
				else{
					//cr�ation du d�placement
					Deplacement deplacement = new Deplacement(temp, new Position(colonneClic,ligneClic));

					if(pieceTampon instanceof Tour && e.roqueValide(deplacement)) {
						if(((Tour) pieceTampon).isPremierMouv()){
							char couleur = couleurControle=="blanc" ? 'B' : 'N';
							if(couleur == 'B') {
								if(temp.getColonne() == 0) {
									tab[3][7].setIcon(new ImageIcon(dossierIcone+"T"+couleur+".gif"));
									tab[2][7].setIcon(new ImageIcon(dossierIcone+"R"+couleur+".gif"));
									e.getCase(3, 7).setPiece(pieceTampon);
									e.getCase(2, 7).setPiece(new Roi("blanc"));	
									((Roi)e.getCase(2, 7).getPiece()).setPremierMouv(false);
									((Tour)e.getCase(3, 7).getPiece()).setPremierMouv(false);
								}
								else {
									tab[5][7].setIcon(new ImageIcon(dossierIcone+"T"+couleur+".gif"));
									tab[6][7].setIcon(new ImageIcon(dossierIcone+"R"+couleur+".gif"));
									e.getCase(5, 7).setPiece(pieceTampon);
									e.getCase(6, 7).setPiece(new Roi("blanc"));	
									((Roi)e.getCase(6, 7).getPiece()).setPremierMouv(false);
									((Tour)e.getCase(5, 7).getPiece()).setPremierMouv(false);
								}
							}
							else {
								if(temp.getColonne() == 0) {
									tab[3][0].setIcon(new ImageIcon(dossierIcone+"T"+couleur+".gif"));
									tab[2][0].setIcon(new ImageIcon(dossierIcone+"R"+couleur+".gif"));
									e.getCase(3, 0).setPiece(pieceTampon);
									e.getCase(2, 0).setPiece(new Roi("noir"));	
									((Roi)e.getCase(2, 0).getPiece()).setPremierMouv(false);
									((Tour)e.getCase(3, 0).getPiece()).setPremierMouv(false);
								}
								else {
									tab[5][0].setIcon(new ImageIcon(dossierIcone+"T"+couleur+".gif"));
									tab[6][0].setIcon(new ImageIcon(dossierIcone+"R"+couleur+".gif"));
									e.getCase(5, 0).setPiece(pieceTampon);
									e.getCase(6, 0).setPiece(new Roi("noir"));	
									((Roi)e.getCase(6, 0).getPiece()).setPremierMouv(false);
									((Tour)e.getCase(5, 0).getPiece()).setPremierMouv(false);
								}								
							}
							
							tab[temp.getColonne()][temp.getLigne()].setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0),0));
							tab[colonneClic][ligneClic].setIcon(null);
							tab[temp.getColonne()][temp.getLigne()].setIcon(null);
							e.getCase(temp.getColonne(), temp.getLigne()).setPiece(null);
							e.getCase(colonneClic, ligneClic).setPiece(null);
							
							pieceTampon = null;
							iconeTampon = null;
							temp = null;

							couleurControle = couleurControle.equals("blanc") ? "noir" : "blanc";		
						}	
					}	
					// si la pi�ce respecte son d�placement, d�placement possible ou si le pion peut manger
					else if ((pieceTampon.estValide(deplacement) && e.estPossible(deplacement)) | e.capturePionPossible(deplacement)){
							
						//Cas promotion
						if((ligneClic == 0 || ligneClic == 7  )&& pieceTampon.getNom()== "Pion") {
							pieceTampon = new Dame(couleurControle);
							char couleur = couleurControle=="blanc" ? 'B' : 'N';
							iconeTampon = new ImageIcon(dossierIcone+"D"+couleur+".gif");
						}
						

						
						// tempporaire : condition fin de partie : roi manger
						if(e.getCase(colonneClic, ligneClic).getPiece() instanceof Roi){
							if(JOptionPane.showConfirmDialog(null, "F�licitation vous avez gagn�! \n", "BRAVO !", JOptionPane.DEFAULT_OPTION) == 0){
									retour();
									tab[temp.getColonne()][temp.getLigne()].setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0),0));
								}
							}
						else { // si la case est vide
							
							if(pieceTampon.getNom() == "Roi") {
								((Roi)pieceTampon).setPremierMouv(false);
							}
							
							if(pieceTampon.getNom() == "Tour") {
								((Tour)pieceTampon).setPremierMouv(false);
							}

							// tampon sur la case puis vide le tampon
							e.getCase(temp.getColonne(), temp.getLigne()).setPiece(null);
							tab[temp.getColonne()][temp.getLigne()].setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0),0));
	
							tab[colonneClic][ligneClic].setIcon(iconeTampon);
							e.getCase(colonneClic, ligneClic).setPiece(pieceTampon);
							tab[temp.getColonne()][temp.getLigne()].setIcon(null);
	
	
							pieceTampon = null;
							iconeTampon = null;
							temp = null;
	
							couleurControle = couleurControle.equals("blanc") ? "noir" : "blanc";

						}
					}
					else{
						tab[temp.getColonne()][temp.getLigne()].setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0),0));
						pieceTampon = null;
						iconeTampon = null;
						temp = null;

						}						
					}
				}					
			}			
			if (eve.getClickCount() == 2) {			
				if (eve.getSource() instanceof JLabel) // donc on a cliqu� sur un Label
				{
					//sur quel label on double clique
					for (int i = 0; i < 8; i++) {
						for (int j = 0; j < 8; j++) {
							if (eve.getSource() == tab[j][i]) {
								ligneClic2 = i;
								colonneClic2 = j;
							}
						}
					}		
				}
				if((e.getCase(colonneClic2, ligneClic2).getPiece() != null )){

					Deplacement[][] deplacement=new Deplacement[8][8] ;
				   
					//initialisation
					piDoubleClick = e.getCase(colonneClic2, ligneClic2).getPiece();
					poDoubleClick= new Position(colonneClic2,ligneClic2);
				   
					for (int i = 0; i < 8; i++) {
						for (int j = 0; j < 8; j++) {
						    deplacement[j][i]= new Deplacement(poDoubleClick, new Position(j,i));
						    
						    if ((piDoubleClick.estValide(deplacement[j][i]) && e.estPossible(deplacement[j][i])) | e.capturePionPossible(deplacement[j][i]) ) {
						    	
						    	tab[j][i].setBorder(BorderFactory.createLineBorder((Color.YELLOW),5));
						    	
						    }
						} 
					}						
				}							
			}		
		}
		public void mousePressed(MouseEvent eve) {
			
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						tab[j][i].setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0),0));
					}	
				}
		}//fin pressed	
	}
}
