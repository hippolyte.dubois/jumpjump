package menu;

// une case de l'echiquier
public class Case {
	
	private Piece piece;
	
	public Case() {}
	
	public Case(Piece p) {
		piece = p;
	}

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	
	public boolean estOccupe() {
		return (piece !=null);
	}
	
	public boolean estOccupe(String couleur) {
		if(piece == null)
			return false;
		else
			return (piece.getColor().equals(couleur));
	}
	
}
