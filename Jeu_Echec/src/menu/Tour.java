package menu;

public class Tour extends Piece {	
	
	private boolean premierMouv;
	
	public Tour(String color) {
		super("Tour", color);
		premierMouv = true;
	}
	
	public boolean estValide(Deplacement deplacement) {
		
			return (deplacement.getDepX() == 0 || deplacement.getDepY() == 0) && !deplacement.Pasdep();		
	}

	public boolean isPremierMouv() {
		return premierMouv;
	}

	public void setPremierMouv(boolean premierMouv) {
		this.premierMouv = premierMouv;
	}
	
	
}

