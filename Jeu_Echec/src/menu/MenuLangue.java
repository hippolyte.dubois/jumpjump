package menu;

import javax.swing.*;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.*;



//Menu de s�lection de la langue (base : Fran�ais)
public class MenuLangue extends JFrame {
	
	private JButton btnFr;
	private JButton btnEn;
	private JButton btnPr;
	private JButton btnRetour;
	private String langue="Regle.txt";
	
	
	public MenuLangue(String langue)
	{
		this.langue=langue;
		
		setTitle("Chess Game");//donne un titre
		setSize(400, 350);//400px/350px
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		setLayout(new GridLayout(4,0));
		add(btnFr = new JButton("Fran�ais"));
		add(btnEn = new JButton("Anglais"));
		add(btnPr = new JButton("Portuguais"));
		add(btnRetour = new JButton("Retour"));
		
		//gestion des evenement
		GestionnaireEvent gest = new GestionnaireEvent();		
		btnFr.addMouseListener(gest);
		btnEn.addMouseListener(gest);
		btnPr.addMouseListener(gest);
		btnRetour.addMouseListener(gest);
		if(langue=="Regle.txt") {
			btnFr.setEnabled(false);
		}			
		else if(langue=="ReglePT.txt") {
			btnPr.setEnabled(false);
		}
		else {
			btnEn.setEnabled(false);
		}
			
		setVisible(true);
	}
	public void retour() {
		this.dispose();
		new Menu(langue);
	}
	
	public String getLangue()
	{
		return langue;
	}
	
	public void setLangue(String langue)
	{
		this.langue=langue;
	}
	
	

	
	//class qui execute les evenement
	public class GestionnaireEvent extends MouseAdapter{
		

		public void mouseClicked(MouseEvent eve) {
			
			if (eve.getSource() == btnRetour){
				retour();
			}
			if (eve.getSource()==btnFr)
			{
				langue="Regle.txt";
				btnPr.setEnabled(true);
				btnFr.setEnabled(false);
				btnEn.setEnabled(true);
				
			}
			else
				if (eve.getSource()==btnPr)
				{
					langue="ReglePT.txt";
					btnPr.setEnabled(false);
					btnFr.setEnabled(true);
					btnEn.setEnabled(true);
			
				}
			else
				if (eve.getSource()==btnEn)
				{
					langue="RegleEN.txt";
					btnPr.setEnabled(true);
					btnFr.setEnabled(true);
					btnEn.setEnabled(false);
				}
		}
	}
}
