package menu;

public class Roi extends Piece {

	private boolean premierMouv;
	
	public Roi(String color) {
		super("Roi", color);
		premierMouv = true;
	}
	
	public boolean estValide(Deplacement deplacement) {
			return deplacement.getDepX() <= 1 && deplacement.getDepY() <= 1 &&
					deplacement.getDepX() >= -1 && deplacement.getDepY() >= -1 && !deplacement.Pasdep();
	}

	public boolean isPremierMouv() {
		return premierMouv;
	}

	public void setPremierMouv(boolean premierMouv) {
		this.premierMouv = premierMouv;
	}
	
	
}

