package menu;

public class Fou extends Piece {	
	
	public Fou(String color) {
		super("Fou", color);
	}
	
	public boolean estValide(Deplacement deplacement) {
		return Math.abs(deplacement.getDepX()) == Math.abs(deplacement.getDepY()) && !deplacement.Pasdep();
	}
}
