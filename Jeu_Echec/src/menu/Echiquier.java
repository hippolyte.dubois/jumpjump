package menu;


// Moteur, represente l'echiquier de fa�on informatique
public class Echiquier {
	
	private Case [][] echiquier;
	
	public Echiquier() {
		echiquier = new Case[8][8];
		for (int ctr = 0; ctr <= 7; ctr++)
			for (int ctr1 = 0; ctr1 <= 7; ctr1++)
				echiquier[ctr][ctr1] = new Case();		
	}
	
	public Case getCase(int col, int lig){
		return echiquier[col][lig];
	}
	
	public boolean capturePionPossible(Deplacement d) {//verrifie si un pion peut manger une pi�ce
		if(echiquier[d.getDepart().getColonne()][d.getDepart().getLigne()].getPiece() instanceof Pion)
		{

			Case Arrive = echiquier[(int)d.getArrivee().getColonne()][(int)d.getArrivee().getLigne()];
			String couleurDepart = echiquier[(int)d.getDepart().getColonne()][(int)d.getDepart().getLigne()].getPiece().getColor();
			

			if(Arrive.estOccupe(couleurDepart.equals("blanc") ? "noir" : "blanc"))

				return (d.getDepY() * Math.abs(d.getDepX()) == (couleurDepart.equals("noir") ? 1 : -1));
		}
		return false;
	}
	
	public boolean estPossible(Deplacement d) {		// verifie que la piece peut ce d�placer

		
		Piece pieceDepart = echiquier[(int)d.getDepart().getColonne()][(int)d.getDepart().getLigne()].getPiece();
		
		//case libre ou piece adverse
		if (!echiquier[(int)d.getArrivee().getColonne()][(int)d.getArrivee().getLigne()].estOccupe(pieceDepart.getColor().equals("blanc") ? "blanc" : "noir")
				|| d.Pasdep()){
			if (!(pieceDepart instanceof Cavalier)){//cas Cavalier
				if(!(pieceDepart instanceof Pion)){//cas Pion
					//d�placement > 1
					if(!(Math.abs(d.getDepX()) - Math.abs(d.getDepY()) <= 1 && Math.abs(d.getDepX()) + Math.abs(d.getDepY()) <= 1)){

						//JumpX et jumpY seront sois 0,  1 ou -1, seervent a v�rifier les cases entre le d�part et l'arriv�
						int jumpX = d.getDepX() == 0 ? 0 : (int)(d.getArrivee().getColonne() - d.getDepart().getColonne())
								/Math.abs((int)(d.getArrivee().getColonne() - d.getDepart().getColonne()));
				
						int jumpY = d.getDepY() == 0 ? 0 : (int)(d.getArrivee().getLigne() - d.getDepart().getLigne())
								/Math.abs((int)(d.getArrivee().getLigne() - d.getDepart().getLigne()));
						//verification des case entre d�part et arriv�
						for (int ctrX = (int)d.getDepart().getColonne() + jumpX, ctrY = (int)d.getDepart().getLigne() + jumpY;
								ctrX != (int)d.getArrivee().getColonne() | ctrY != (int)d.getArrivee().getLigne();
								ctrX += jumpX, ctrY += jumpY){
								if (echiquier[ctrX][ctrY].estOccupe()){
									return false;
								}
						}
						return true;
					}
					else
						//test precedent vrai donc d�placement possible
						return true;
				}
				else
					//cas du pion
					return !echiquier[(int)d.getArrivee().getColonne()][(int)d.getArrivee().getLigne()].estOccupe();
					
			}
			else
				//Cas du cavalier
				return true;
		}
		else
			return false;

		
	}
	
	public boolean roqueValide(Deplacement d) {
		if(echiquier[(int)d.getArrivee().getColonne()][(int)d.getArrivee().getLigne()].getPiece() instanceof Roi) {
			if(((Roi)echiquier[(int)d.getArrivee().getColonne()][(int)d.getArrivee().getLigne()].getPiece()).isPremierMouv()){
				if(!(Math.abs(d.getDepX()) - Math.abs(d.getDepY()) <= 1 && Math.abs(d.getDepX()) + Math.abs(d.getDepY()) <= 1)){
					//JumpX et jumpY seront sois 0,  1 ou -1, seervent a v�rifier les cases entre le d�part et l'arriv�
					int jumpX = d.getDepX() == 0 ? 0 : (int)(d.getArrivee().getColonne() - d.getDepart().getColonne())
							/Math.abs((int)(d.getArrivee().getColonne() - d.getDepart().getColonne()));
			
					int jumpY = d.getDepY() == 0 ? 0 : (int)(d.getArrivee().getLigne() - d.getDepart().getLigne())
							/Math.abs((int)(d.getArrivee().getLigne() - d.getDepart().getLigne()));
					//verification des case entre d�part et arriv�
					for (int ctrX = (int)d.getDepart().getColonne() + jumpX, ctrY = (int)d.getDepart().getLigne() + jumpY;
							ctrX != (int)d.getArrivee().getColonne() | ctrY != (int)d.getArrivee().getLigne();
							ctrX += jumpX, ctrY += jumpY){
							if (echiquier[ctrX][ctrY].estOccupe()){
								return false;
							}
					}
					return true;	
				}
				else
					return true;	
			}
			else
				return false;			
		}
		else
			return false;
	}

			
	
	
	public void initialiser() {		// placer les pi�ce sur l'echiqier

		int ligne = 7;

		for (String couleur = "noir" ; !couleur.equals("blanc"); couleur = "blanc", ligne = 0){

			echiquier[0][ligne].setPiece(new Tour(couleur));
			echiquier[1][ligne].setPiece(new Cavalier(couleur));
			echiquier[2][ligne].setPiece(new Fou(couleur));
			echiquier[3][ligne].setPiece(new Dame(couleur));
			echiquier[4][ligne].setPiece(new Roi(couleur));
			echiquier[5][ligne].setPiece(new Fou(couleur));
			echiquier[6][ligne].setPiece(new Cavalier(couleur));
			echiquier[7][ligne].setPiece(new Tour(couleur));
			ligne += couleur.equals("noir") ? -1 : 1;
			for (int ctr = 0; ctr <= 7; ctr++)
				echiquier[ctr][ligne].setPiece(new Pion(couleur));
		}
	}
}	
	
	
