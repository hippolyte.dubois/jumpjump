package menu;
// repr�senter une position sur l'�chiquier
public class Position {
	
	private int ligne;
	private int colonne;
	

	public Position(int colonne , int ligne) {

		this.ligne = ligne;
		this.colonne = colonne;
	}

	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}
	
	public boolean egale (Position p) {
		return p.getColonne() == this.getColonne() &&
				p.ligne == this.ligne;
	}
	

}
