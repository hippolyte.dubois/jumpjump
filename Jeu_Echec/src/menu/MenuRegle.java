package menu;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

// affiche le menu des regles
public class MenuRegle extends JFrame{
	
	private JLabel lblRegle ;
	private JButton btnRetour ;
	private String StrRegle;
	
	MenuRegle(String langue) throws IOException
	{
		StrRegle=langue;
		setTitle("Chess Game");//donne un titre
		setSize(650, 575);//650px/575px
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		
		//transformation d'un .txt en string 
		FileInputStream regle = new FileInputStream(StrRegle);		
		BufferedReader buf = new BufferedReader(new InputStreamReader(regle)); 
		String line = buf.readLine();
		StringBuilder sb = new StringBuilder(); 
		
		while(line != null) {
			
			sb.append(line +"<br/>");
			line = buf.readLine(); 
			
		} 
		buf.close();
		


		btnRetour = new JButton("Retour");

		//ajout du contenus du fichier au string 		
		lblRegle = new JLabel("<html> <br> "+sb.toString()+"</br> </html>");
		lblRegle.setBorder(new CompoundBorder(lblRegle.getBorder(), new EmptyBorder(0,5,0,0)));
		//ajout de la fct btnRetour
		GestionnaireEvent gest = new GestionnaireEvent();	
		btnRetour.addMouseListener(gest);
	
		//ajout du contenus dans une fenetre
		setLayout(new BorderLayout(1,0));
		add(btnRetour,BorderLayout.SOUTH);
		add(lblRegle);
		
		setVisible(true);
	}
	
	
	//fct de retour au menu
	public void retour() {
		this.dispose();
		new Menu(StrRegle);
	}
	
	//class qui execute les evenement
	public class GestionnaireEvent extends MouseAdapter{
		
			public void mouseClicked(MouseEvent eve) {
				
				if (eve.getSource() == btnRetour){
					
					retour();
				}
			
			}		
	}
}