package menu;

// classe commune pour creer les autre piece du jeu
public abstract class Piece {
	
	private String nom;
	private String color;
	
	public Piece(String nom, String color) {
		setNom(nom);
		setColor(color);
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		if ((color == "noir") || (color == "blanc"))
			this.color = color;
	}

	public abstract boolean estValide(Deplacement deplacement);
}
